import React from "react";
import Header from "../header/header";
import Footer from "../footer/footer";
export default class MainTemplate extends React.Component{ 
   constructor(props){ 
      super(props); 
   }
   render(){ 
       return( 
          <div> 
             <Header/> 
             <div class="container">
             {this.props.children} 
             </div>
             <Footer/> 

         </div>
      ); 
   }
}