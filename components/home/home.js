import React from "react";
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom'
import './home.css';


export default class Home extends React.Component{ 
   constructor(props){ 
      super(props); 
   }
   registrato(){
      if(!window.localStorage.username){
          
        }else{
         return <Redirect to='/target' />
        }
  }
   render(){ 
      this.registrato()
      return( 
        
<div class="row">
  <h5 class="marg">Benvenuto 👋</h5>
   <p>Il magneti cowork è uno spazio condiviso nel pieno centro di Palermo!</p>
   <p>Non hai ancora letto il <Link class="white-text" to="/regolamento">📜regolamento?</Link> </p>
   <p class="marg col s4 offset-s4 waves-effect waves-light btn"><Link class="white-text" to="/accedi">Accedi</Link></p>
</div>




      ); 
   } 
}