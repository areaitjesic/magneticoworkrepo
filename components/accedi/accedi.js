import React from "react";
import './accedi.css';
import { Redirect, Link } from 'react-router-dom'


export default class Accedi extends React.Component{ 
   constructor(props){ 
      super(props); 
      this.accedi = this.accedi.bind(this);
   }
   state = {
    toProfilo: false,
  }

   async accedi(){
    console.log("accedo")
      let user = document.querySelector("#username").value
      let pass = document.querySelector("#password").value

      const l = async () => {
        const response = await fetch('https://magneticowork.altervista.org/webapp/funzioni/login.php?username='+user+'&pass='+pass);
        const data = await response.json();
        console.log(data)
        if(data === "Logged"){
        window.localStorage.setItem("username", user)
        window.localStorage.setItem("pass", pass)
        this.setState(() => ({
          toProfilo: true
        }))
        }else{
          window.M.toast({html: "i dati inseriti sono errati"})
          return false
        }
    }
l()
}


registrato(){
  if(window.localStorage.username){
    this.setState(() => ({
      toProfilo: true
    }))
    }else{
      
    }
}

    
sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

   render(){ 
    if (this.state.toProfilo === true) {
      return <Redirect to='/profilo' />
    }

    this.registrato()


    
      return(
          
          <div class="marg">

      <h5>Effettua il login 👋</h5>
      <div class="input-field col s12">
              <input id="username" type="text" class="white-text validate" />
              <label for="username">Username</label>
            </div>
    
    
            <div class="input-field col s12">
              <input id="password" type="password" class="white-text validate" />
              <label for="password">Password</label>
            </div>
       
       
    <a onClick={() => { this.accedi() }} class="waves-effect waves-light btn">accedi</a>     
    
    <p class="bott">Non sei ancora registrato? <Link class="" to="/registrati">📎 registrati</Link></p>

    
    
    </div>



      ); 
   } 
}