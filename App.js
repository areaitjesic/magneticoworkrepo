import React, { Component } from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom';
 
import MainTemplate from "./components/mainLayout/template/mainTemplate";
import Home from "./components/home/home";
import Accedi from "./components/accedi/accedi";
import Profilo from "./components/profilo/profilo";
import Qrcodereader from "./components/qrcodereader/qrcodereader";
import Acquisti from "./components/acquisti/acquisti";
import Registrati from "./components/registrati/registrati";
 
class App extends Component {
 
   constructor(props){
      super(props);
   }
 
   render() {
 
      return (
         <BrowserRouter>
            <MainTemplate>
               <Switch>
                  //Parte pubblica

                        <Route exact path='/' component={Home}/>
                        <Route exact path='/registrati' component={Registrati}/> 
                        <Route exact path='/accedi' component={Accedi}/>

                  //!Parte pubblica



                  //Parte privata con semplice login

                        <Route exact path='/profilo' component={Profilo}/>
                        <Route exact path='/qrcodereader' component={Qrcodereader}/> 

                  //!Parte privata con semplice login



                  //Parte privata con login e qrcode(Sono dentro il magneti)

                        <Route exact path='/acquisti' component={Acquisti}/> 

                  //!Parte privata con login e qrcode(Sono dentro il magneti)
                   

                   
               </Switch>
            </MainTemplate>
         </BrowserRouter>
      );
   }
}

export default App;